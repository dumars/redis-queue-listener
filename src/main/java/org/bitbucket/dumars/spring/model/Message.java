package org.bitbucket.dumars.spring.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class Message implements Serializable {

	private static final long serialVersionUID = -4289200901554157919L;
	private String receiver;
	private String content;
	private Date sendedDate;
	
}
