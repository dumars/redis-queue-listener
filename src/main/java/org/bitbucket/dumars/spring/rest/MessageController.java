package org.bitbucket.dumars.spring.rest;

import java.util.Date;

import org.bitbucket.dumars.spring.Application;
import org.bitbucket.dumars.spring.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class MessageController {
	
	@Autowired
	private RedisTemplate<String, Message> redisTemplate;
	
	@RequestMapping(value="/redis/queue", method = RequestMethod.POST)
	public Long add() {
		log.debug("add a new message.");
		
		Message message = new Message();
		message.setReceiver("Dumars");
		message.setContent("Hello Redis!");
		message.setSendedDate(new Date());
		
		return redisTemplate.opsForList().rightPush(Application.KEY, message);
	}
	
	@RequestMapping(value="/redis/queues", method = RequestMethod.POST)
	public Long addAll() {
		log.debug("add new messages.");
		
		Message message = new Message();
		message.setReceiver("Dumars");
		message.setContent("Hello Redis!");
		message.setSendedDate(new Date());
		
		Message message2 = new Message();
		message2.setReceiver("Tsai");
		message2.setContent("Hello Redis!");
		message2.setSendedDate(new Date());
		
		return redisTemplate.opsForList().rightPushAll(Application.KEY, new Message[]{message, message2});
	}
}
