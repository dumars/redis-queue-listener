package org.bitbucket.dumars.spring.redis.listener;

import org.bitbucket.dumars.spring.redis.RedisListenerException;

public interface RedisListener<T> {

	public void onEvent(T value) throws RedisListenerException;
}
