package org.bitbucket.dumars.spring.redis;

import java.util.List;

import org.bitbucket.dumars.spring.Application;
import org.bitbucket.dumars.spring.model.Message;
import org.bitbucket.dumars.spring.redis.listener.RedisListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class MessageQueueFactoryBean implements InitializingBean, DisposableBean {

	@Autowired
	private RedisTemplate<String, Message> redisTemplate;
	@Autowired
	protected RedisListener<Message> listener;

	protected RedisConnection connection;
	protected RedisSerializer<Message> serializer;
	volatile boolean terminate = false;
	private RedisListenerThread thread;

	@Override
	@SuppressWarnings("unchecked")
	public void afterPropertiesSet() throws Exception {
		this.connection = redisTemplate.getConnectionFactory().getConnection();
		this.serializer = (RedisSerializer<Message>)redisTemplate.getValueSerializer();
		this.thread = new RedisListenerThread();
		this.thread.setDaemon(true);
		this.thread.start();
	}
	
	@Override
	public void destroy() throws Exception {
		log.info("MessageRespository destroy....");
		terminate = true;
		
		if(thread != null)
			thread.interrupt();
		
		connection.close();
	}
	
	class RedisListenerThread extends Thread {
		
		private Logger logger = LoggerFactory.getLogger(RedisListenerThread.class);
		
		@Override
		public void run() {
			while(!terminate) {
				Message message = pop();
				if (message != null) {
					try {
						listener.onEvent(message);
					} catch (RedisListenerException e) {
						logger.error(e.getMessage());
						backup(message);
					}
				}
			}
		}

		private Message pop() {
			List<byte[]> list = connection.bLPop(1, Application.KEY.getBytes());
			if (!CollectionUtils.isEmpty(list)) {
				return serializer.deserialize(list.get(1));
			}
			return null;
		}
		
		private void backup(Message message) {
			log.info("move error data to backup.");
			connection.rPush(Application.KEY_BACKUP.getBytes(), serializer.serialize(message));
		}
	}
}
