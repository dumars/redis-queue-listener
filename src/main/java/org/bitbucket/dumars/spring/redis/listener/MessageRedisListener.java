package org.bitbucket.dumars.spring.redis.listener;

import java.util.Random;

import org.bitbucket.dumars.spring.model.Message;
import org.bitbucket.dumars.spring.redis.RedisListenerException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageRedisListener implements RedisListener<Message> {
	
	private final Random random = new Random();

	@Override
	public void onEvent(Message message) throws RedisListenerException {
		if(random.nextBoolean())
			log.info("listener do event, send message: {}", message.toString());
		else
			throw new RedisListenerException("error!!!");
	}

}
