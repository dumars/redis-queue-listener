package org.bitbucket.dumars.spring.redis;

public class RedisListenerException extends Exception {

	private static final long serialVersionUID = -142803514317515819L;

	public RedisListenerException() {
		super();
	}

	public RedisListenerException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RedisListenerException(String message, Throwable cause) {
		super(message, cause);
	}

	public RedisListenerException(String message) {
		super(message);
	}

	public RedisListenerException(Throwable cause) {
		super(cause);
	}

}
