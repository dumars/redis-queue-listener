package org.bitbucket.dumars.spring.schedule;

import org.bitbucket.dumars.spring.Application;
import org.bitbucket.dumars.spring.model.Message;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestoreMessageJob {
	
	@Setter
	private RedisTemplate<String, Message> redisTemplate;

	@Scheduled(cron="*/30 * * * * ?")
	public void run() {
		Long size = redisTemplate.opsForList().size(Application.KEY_BACKUP);
		if(size != null && size > 0) {
			log.info("restore {} message(s).", size);
			for(int i = 0; i < size; i++) {
				redisTemplate.opsForList().rightPopAndLeftPush(Application.KEY_BACKUP, Application.KEY);
			}
		}
		
	}
}
