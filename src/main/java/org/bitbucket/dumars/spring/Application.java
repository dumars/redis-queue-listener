package org.bitbucket.dumars.spring;

import org.bitbucket.dumars.spring.model.Message;
import org.bitbucket.dumars.spring.redis.listener.MessageRedisListener;
import org.bitbucket.dumars.spring.schedule.RestoreMessageJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {
	
	public final static String KEY = "NOTIFICATION:MESSAGE";
	public final static String KEY_BACKUP = "NOTIFICATION:MESSAGE_BACKUP";
	
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
	    JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
	    jedisConFactory.setHostName("localhost");
	    jedisConFactory.setPort(6379);
	    return jedisConFactory;
	}
	
	@Bean
	public RedisTemplate<String, Message> redisTemplate() {
	    RedisTemplate<String, Message> template = new RedisTemplate<String, Message>();
	    template.setConnectionFactory(jedisConnectionFactory());
	    template.setKeySerializer(new StringRedisSerializer());
	    template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
	    return template;
	}
	
	@Bean
	public MessageRedisListener messageRedisListener() {
		return new MessageRedisListener();
	}
	
	@Bean
	public RestoreMessageJob restoreMessageJob() {
		RestoreMessageJob job = new RestoreMessageJob();
		job.setRedisTemplate(redisTemplate());
		return job;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
